import React from 'react';
import Avatar from './Avatar';
import Username from './Username';
import Biodata from './Biodata';


const Userprofile = () => (
    <div>
        <Avatar />
        <Username />
        <Biodata />
    </div>
);

export default Userprofile;