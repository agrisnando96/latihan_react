import React from 'react';

const Username = props => (
  <h4 className="bg-primary text-white text-center p-2">{props.Username}</h4>
);

Username.defaultProps = {
  Username: "Agris"
};

export default Username;