import React from 'react';
import './App.css';
import Greetings from './Greeting';
import Clock from './clock';
import Userprofile from './Userprofile';

function App() {
  return (
    <div className="App">

      <Greetings name="Agris Grisnando"/>
      <Greetings age="23"/>
      <Greetings gender="Laki-laki"/>
    
      <Userprofile />
      <Clock />

    </div>
  );
}

export default App;
