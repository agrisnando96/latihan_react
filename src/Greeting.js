import React from 'react';

const Greetings = props => (
    <h1 className="bg-primary text-white text-center p-2">{props.name}{props.age}{props.gender}</h1>
);

// Greetings.defaultProps = {
//   name: "User"
// };

export default Greetings