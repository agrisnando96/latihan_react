import React from 'react'

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date() };
    }
    componentDidMount() {
        this.timerID = setInterval(() => this.updateClock(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    } updateClock() {
        this.setState({
            date: new Date()
        });
    }
    render() {
        return (
            <div>
                <h1>Selamat Datang Kawan!</h1>
                <h2>Sekarang Itu Jam {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}

export default Clock